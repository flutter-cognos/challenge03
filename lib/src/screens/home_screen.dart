import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  HomeScreen({Key? key}) : super(key: key);

  final opciones = {
    '/bottomNav': 'Bottom Navigation Bar',
  };

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Challenge #3'),
        backgroundColor: Colors.indigo,
      ),
      body: ListView(children: crearItems(context)),
    );
  }

  List<Widget> crearItems(context) {
    List<Widget> lista = [];
    opciones.forEach((key, value) {
      final tempWidget = ListTile(
        title: Text(value),
        subtitle: Text(key),
        leading: const Icon(Icons.table_rows),
        trailing: const Icon(Icons.arrow_right),
        onTap: () {
          Navigator.pushNamed(context, key);
        },
      );
      lista.add(tempWidget);
    });
    return lista;
  }
}
