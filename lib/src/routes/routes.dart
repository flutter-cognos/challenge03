import 'package:flutter/widgets.dart';

import '../screens/bottom_nav_bar_screen.dart';
import '../screens/data_tables_screen.dart';
import '../screens/home_screen.dart';

Map<String, WidgetBuilder> getRoutes() {
  return {
    '/': (context) => HomeScreen(),
    '/bottomNav': (context) => const BottomNavBarScreen(),
    '/dataTable': (context) => DataTablesScreen(),
  };
}
